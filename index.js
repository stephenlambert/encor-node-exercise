const express = require('express')
const app = express()
const people = require("./people.js")
const planets = require("./planets.js")


app.get("/", function (req, res) {
    res.send("Hello, world")
})

app.get("/people", people.getAllPeople)

app.get("/planets", planets.getAllPlanets)

app.listen(3000)