const axios = require("axios");

async function getAllPeople(req, res) {
    running_list = []

    url_to_call = "https://swapi.dev/api/people"

    // get all the data
    while (url_to_call) {
        try {
            response_from_swapi = await axios.get(url_to_call);
        } catch (e) {
            // error from API, send 502
            res.sendStatus(502);
            return;
        }

        data_returned = response_from_swapi.data

        url_to_call = data_returned.next
        
        running_list = running_list.concat(data_returned.results)
    }

    // sort if needed
    sortByVal = req.query.sortBy

    // function to pass to sort with a mapper function
    function sortPrepFunc(mapper){
        return function (a, b){
            if (mapper(a) < mapper(b)){
                return -1
            }
            else {
                return 1
            }
        }
    }

    if (sortByVal == "name"){
        running_list.sort(sortPrepFunc((item) => item.name))
    }
    else if (sortByVal == "height"){
        mapper_func = function(item){
            height = Number.parseFloat(item.height.replace(",", ""))

            if (Number.isNaN(height)){
                return 0
            }
            else {
                return height
            }
        }
        running_list.sort(sortPrepFunc(mapper_func))
    }
    else if (sortByVal == "mass") {
        mapper_func = function (item) {
            mass = Number.parseFloat(item.mass.replace(",", ""))

            if (Number.isNaN(mass)){
                return 0
            }
            else {
                return mass
            }
        }
        running_list.sort(sortPrepFunc(mapper_func))
    }

    res.json(running_list)
}

exports.getAllPeople = getAllPeople;