const axios = require("axios");

// get resident name from the link for their API call
async function residentLinkToName(resident_link){
    response = await axios.get(resident_link);

    return response.data.name
}

// create a copy of the planet with residents' names in place of links
async function addResidentsForPlanet(planet){
    original_residents = planet.residents

    resident_name_promises = original_residents.map(residentLinkToName)

    resident_names = await Promise.all(resident_name_promises)

    new_planet = Object.assign({}, planet)

    new_planet.residents = resident_names

    return new_planet
}

async function getAllPlanets(req, res) {
    running_list = []

    url_to_call = "https://swapi.dev/api/planets"

    // get all the data
    while (url_to_call) {
        try {
            response_from_swapi = await axios.get(url_to_call);
        } catch (e) {
            // error from API, send 502
            res.sendStatus(502);
            return;
        }

        data_returned = response_from_swapi.data

        url_to_call = data_returned.next
        
        running_list = running_list.concat(data_returned.results)
    }

    let all_planets;

    // swap out resident names for links
    try {
        planets_promise = running_list.map(addResidentsForPlanet)

        all_planets = await Promise.all(planets_promise)
    } catch (e) {
        console.log(e)
        res.sendStatus(502)
    }

    res.json(all_planets)
}

exports.getAllPlanets = getAllPlanets